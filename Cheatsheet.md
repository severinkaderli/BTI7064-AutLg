---
title: "Automaten und Formale Sprachen"
author: "Severin Kaderli"
---

# Formale Sprachen
## Definitionen
**Alphabet:** $\Sigma$, eine endliche Menge von Symbolen.  
**Zeichen:** $\sigma \in \Sigma$  
**Wort:** $\omega \in \Sigma^{*}$  
**Formale Sprache:** $L \subseteq \Sigma^{*}$

## Probleme
**Wortproblem:**  
Gibt es für ein Wort und eine Sprache die Beziehung $\omega \in L$?

**Leehrheitsproblem:**  
Enthält eine Sprache mindestens ein Wort?

**Endlichkeitsproblem:**  
Besitzt eine Sprache endlich viele Elemente?

**Äquivalenzproblem:**  
Gilt für zwei Sprachen die Beziehung $L_{1} = L_{2}$?

**Spracherzeugungsproblem:**  
Gibt es für eine Sprache eine Beschreibung, aus der sich alle Wörter ableiten lassen?

## Grammatik
Ist ein Viertupel ($V, \Sigma, P, S$):  
**V:** Variablenmenge (Nonterminale)  
**$\Sigma$:** Terminalalphabet  
**P:** Produktionen (Regeln)  
**S:** Startvariable

**Beispiel:**  
$G = (\{S\}, \{(,),[,]\}, P, S)$

P: $S \to \epsilon \mid SS \mid [S] \mid (S)$

## Chomsky-Hierarchie
**Reguläre Grammatiken (Typ-3):**  
Links nur eine Variable und Rechts zwei Zeichen (Terminalzeichen + Variable).  
$S \to aT$  
Werden von endlichen Automaten akzeptiert und durch reguläre Ausdrücke dargestellt.

**Kontextfreie Grammatiken (Typ-2):**  
Links muss eine Variable sein. Rechts kann irgendetwas stehen.  
$S \to aSb$  
Werden von Kellerautomaten akzeptiert und können die Syntax der meisten
Programmiersprachen beschreiben.

**Kontextsensitive Grammatiken (Typ-1):**  
Rechte Seite muss grösser oder gleich gross wie die Linke sein.  
$aSb \to aTcb$

**Phasenstrukturgrammatiken (Typ-0):**  
Keine Einschräkung.  
$aSb \to Ta$

## Reguläre Ausdrücke
**Zeichen:**  
**.:** Beliebiges Zeichen  
**[...]:** Positivliste  
**[^...]:** Negativliste  
**[w]:** Buchstaben, Unterstrich  
**[W]:** Ziffer, Sonderzeichen, Leerraum  
**[s]:** Whitespace  
**[S]:** Zeichen, ausser Leerraum  

**Positionen:**  
**^:** Beginn einer Zeile  
**$:** Ende einer Zeile  
**<:** Beginn eines Worts    
**>:** Ende eines Worts    

**Kombinationen:**  
**+:** Mindestens einmal  
**?:** Höchstens einmal  
**\*:** Beliebig oft  
**{n,}:** Mindestens n-mal  
**{n,m}:** Mindestens n-mal, höchstens m-mal  
**{,m}:** Höchstens m-mal  
**|:** Alternative  
**():** Gruppierung  

# Automaten
## Akzeptoren
Nehmen eine Zeichenfolge entgegen und entscheiden ob es ein gültiges Eingabewort
ist. Diese Wörter bilden die, die vom Automaten akzeptierte Sprache.

## Deterministischer endlicher Akzeptor
Ist ein 5-Tupel ($S, \Sigma, \delta, E, s_{0}$):  
**S:** Zustandsmenge  
**$\Sigma$:** Eingabealphabet  
**$\delta$:** Zustandsübergangfunktion  
**$E$:** Endzustände  
**$s_{0}$:** Startzustand  

## Nichtdeterministischer endlicher Akzeptor
Wie ein DEA, aber ein NEA kann mehrere Folgezustände für eine Eingabe haben.

## Nichtdeterministischer endlicher $\epsilon$-Akzeptor
Mit einem $\epsilon$-Übergang kann man direkt auf einen anderen Zustand wechseln.

## Beispiel

![](./assets/finite_state_automata.png)